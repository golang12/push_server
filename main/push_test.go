package main

import (
	"fmt"
	"gitlab.com/golang12/push_server/modal/firebase"
	"io"
	"net"
	"testing"
)

func TestPushTopic(t *testing.T) {
	conn, err := net.Dial("tcp", "127.0.0.1:8000")
	if err != nil {
		panic(err)
	}

	topic := "/topics/test"
	title := "test"
	body := "test"

	pushTopic, err := firebase.GetPushTopic(topic, title, body)
	if err != nil {
		panic(err)
	}

	_, err = conn.Write(pushTopic)
	if err != nil {
		panic(err)
	}
	for {
		b := make([]byte, 1024)
		n, err := conn.Read(b)
		if err != nil && err != io.EOF {
			panic(err)
		} else {
			fmt.Println(string(b[:n]))
		}
		return
	}
}

func TestUnsub(t *testing.T) {
	conn, err := net.Dial("tcp", "127.0.0.1:8000")
	if err != nil {
		panic(err)
	}

	tokens := []string{
		"fTf9W8Ffp6g:APA91bHGLb198DxEGwmwIG-dIGsneEDt8kdK6hB_dJKnefA-aO2awbwgPp9uxMXULS30LQS757--WakdyuMaqt_icELeXavw7-e5Es8itZiuAAfJkFs2KITL3lIgiedWrruzjMQpdBdG",
	}
	topic := "/topics/test"

	unsub, err := firebase.GetUnsubscribe(tokens, topic)
	if err != nil {
		panic(err)
	}

	_, err = conn.Write(unsub)
	if err != nil {
		panic(err)
	}
	for {
		b := make([]byte, 1024)
		n, err := conn.Read(b)
		if err != nil && err != io.EOF {
			panic(err)
		} else {
			fmt.Println(string(b[:n]))
		}
		return
	}
}

func TestSub(t *testing.T) {
	conn, err := net.Dial("tcp", "127.0.0.1:8000")
	if err != nil {
		panic(err)
	}

	tokens := []string{
		"fTf9W8Ffp6g:APA91bHGLb198DxEGwmwIG-dIGsneEDt8kdK6hB_dJKnefA-aO2awbwgPp9uxMXULS30LQS757--WakdyuMaqt_icELeXavw7-e5Es8itZiuAAfJkFs2KITL3lIgiedWrruzjMQpdBdG",
	}
	topic := "/topics/test"
	sub, err := firebase.GetSubscribe(tokens, topic)
	if err != nil {
		panic(err)
	}

	_, err = conn.Write(sub)
	if err != nil {
		panic(err)
	}
	for {
		b := make([]byte, 1024)
		n, err := conn.Read(b)
		if err != nil && err != io.EOF {
			panic(err)
		} else {
			fmt.Println(string(b[:n]))
		}
		return
	}
}

func TestPushToken(t *testing.T) {
	conn, err := net.Dial("tcp", "127.0.0.1:8000")
	if err != nil {
		panic(err)
	}

	tokens := []string{
		"fTf9W8Ffp6g:APA91bHGLb198DxEGwmwIG-dIGsneEDt8kdK6hB_dJKnefA-aO2awbwgPp9uxMXULS30LQS757--WakdyuMaqt_icELeXavw7-e5Es8itZiuAAfJkFs2KITL3lIgiedWrruzjMQpdBdG",
	}
	title := "tesdfgfdgt"
	body := "tesdfgdfgdfgt"

	pushToken, err := firebase.GetPushToken(tokens, title, body)
	if err != nil {
		panic(err)
	}

	_, err = conn.Write(pushToken)
	if err != nil {
		panic(err)
	}
	for {
		b := make([]byte, 1024)
		n, err := conn.Read(b)
		if err != nil && err != io.EOF {
			panic(err)
		} else {
			fmt.Println(string(b[:n]))
		}
		return
	}
}
