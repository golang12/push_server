package main

import (
	"gitlab.com/golang12/push_server/firebase"
	service "gitlab.com/golang12/push_server/handler"
	"gitlab.com/golang12/push_server/parser"
	"gitlab.com/golang12/push_server/request"
	"os"
)

func main() {
	err := os.Setenv("FIREBASE_ENV","key=")
	if err != nil {
		panic(err)
	}

	data := map[string]interface{}{
		"host":"localhost",
		"db":"",
		"port":"3306",
		"username":"",
		"password":"",
	}
	go firebase.HandlerInvalidToken(data)
	p := parser.JsonParser{}
	gen := request.GenNetRequest{}
	handler := service.Handler{Gen: gen, Parser: p}
	handler.Handle()
}
