package service

import (
	"bytes"
	"fmt"
	"gitlab.com/golang12/push_server/firebase"
	firebase2 "gitlab.com/golang12/push_server/modal/firebase"
	"gitlab.com/golang12/service/parser"
	"gitlab.com/golang12/service/request"
	"io"
	"net"
	"os"
)

type Handler struct {
	Gen    request.GenRequest
	Parser parser.Parser
}

func makeTCPAddr(ip string, port int) (*net.TCPListener, error) {
	addr := net.TCPAddr{}
	addr.IP = net.ParseIP(ip)
	addr.Port = port
	ln, err := net.ListenTCP("tcp", &addr)
	return ln, err
}

func (h *Handler) Handle() {
	ln, err := makeTCPAddr("localhost", 8000)
	if err != nil {
		panic(err)
	}
	for {
		conn, err := ln.AcceptTCP()
		if err != nil {
			panic(err)
		}
		go h.handlerConn(conn)
	}
}

func (h *Handler) handlerConn(conn *net.TCPConn) {

	if err != nil {
		if err == io.EOF {
		}
		conn.Write([]byte(err.Error()))
		conn.Close()
	}
	msg := &firebase2.Message{}
	err = h.Parser.Unmarshal(buf.Bytes(), msg)
	fmt.Println(msg)
	os.Exit(100)
	if err != nil {
		conn.Write([]byte("Parser, Unmarshal: " + err.Error()))
		conn.Close()
	} else {
		nr := h.Gen.New()
		err := firebase.Distribution(msg, nr)
		if err != nil {
			conn.Write([]byte("Distribution,Unmarshal: " + err.Error()))
			conn.Close()
		} else {
			conn.Close()
		}
	}
}
