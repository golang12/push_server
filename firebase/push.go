package firebase

import (
	"encoding/json"
	"gitlab.com/golang12/push_server/logs"
	"gitlab.com/golang12/push_server/modal/firebase"
	"gitlab.com/golang12/service/request"
	"net/http"
	"os"
	"strconv"
)

type ActionSubUnsub struct {
	Request     request.Request
	Body  []byte
	Sub   *firebase.Subscribe
	Unsub *firebase.Unsubscribe
}

func (asu *ActionSubUnsub) Do() {
	if asu.Sub != nil {
		subscribe(asu.Request, asu.Body, asu.Sub)
	} else {
		unsubscribe(asu.Request, asu.Body, asu.Unsub)
	}
}

func PushToken(r request.Request, body []byte, pt *firebase.PushToken) {
	r.SetMethod(http.MethodPost)
	r.SetUrl("https://fcm.googleapis.com/fcm/send")
	r.SetHeader("Authorization", os.Getenv("FIREBASE_ENV"))
	r.SetHeader("Content-Type", "application/json")
	r.SetBody(body)
	err, resp := r.Do()
	if err != nil {
		str := err.Error()
		logs.LogPush(str, "/var/www/push/logs/system.out")
	} else {
		if resp.GetCode() != 200 {
			b, _ := resp.GetBody()
			str := strconv.Itoa(resp.GetCode()) + "\n" + string(b[:]) + "\n" + string(body[:])
			logs.LogPush(str, "/var/www/push/logs/push_token.out")
		} else {
			b, _ := resp.GetBody()
			fcmResponse := &firebase.Response{}
			json.Unmarshal(b, fcmResponse)
			for k, v := range fcmResponse.Result {
				if _, ok := v["error"]; ok {
					DeleteBroadcast <- pt.Tokens[k]
				}
			}
		}
	}
}

func PushTopic(r request.Request, body []byte) {
	r.SetMethod(http.MethodPost)
	r.SetUrl("https://fcm.googleapis.com/fcm/send")
	r.SetHeader("Authorization", os.Getenv("FIREBASE_ENV"))
	r.SetHeader("Content-Type", "application/json")
	r.SetBody(body)
	err, resp := r.Do()
	if err != nil {
		str := err.Error()
		logs.LogPush(str, "/var/www/push/logs/system.out")
	} else {
		if resp.GetCode() != 200 {
			b, _ := resp.GetBody()
			str := strconv.Itoa(resp.GetCode()) + "\n" + string(b[:]) + "\n" + string(body[:])
			logs.LogPush(str, "/var/www/push/logs/push_token.out")
		}
	}
}

func subscribe(r request.Request, body []byte, sub *firebase.Subscribe) {
	r.SetMethod(http.MethodPost)
	r.SetUrl("https://iid.googleapis.com/iid/v1:batchAdd")
	r.SetHeader("Authorization", os.Getenv("FIREBASE_ENV"))
	r.SetHeader("Content-Type", "application/json")
	r.SetBody(body)
	err, resp := r.Do()
	if err != nil {
		str := err.Error()
		logs.LogPush(str, "/var/www/push/logs/system.out")
	} else {
		if resp.GetCode() != 200 {
			b, _ := resp.GetBody()
			str := strconv.Itoa(resp.GetCode()) + "\n" + string(b[:]) + "\n" + string(body[:])
			logs.LogPush(str, "/var/www/push/logs/sub.out")
		} else {
			b, _ := resp.GetBody()
			fcmResponse := &firebase.Response{}
			json.Unmarshal(b, fcmResponse)
			for k, v := range fcmResponse.Result {
				if _, ok := v["error"]; ok {
					DeleteBroadcast <- sub.Tokens[k]
				}
			}
		}
	}
}

func unsubscribe(r request.Request, body []byte, unsub *firebase.Unsubscribe) {
	r.SetMethod(http.MethodPost)
	r.SetUrl("https://iid.googleapis.com/iid/v1:batchRemove")
	r.SetHeader("Authorization", os.Getenv("FIREBASE_ENV"))
	r.SetHeader("Content-Type", "application/json")
	r.SetBody(body)
	err, resp := r.Do()
	if err != nil {
		str := err.Error()
		logs.LogPush(str, "/var/www/push/logs/system.out")
	} else {
		if resp.GetCode() != 200 {
			b, _ := resp.GetBody()
			str := strconv.Itoa(resp.GetCode()) + "\n" + string(b[:]) + "\n" + string(body[:])
			logs.LogPush(str, "/var/www/push/logs/unsub.out")
		} else {
			b, _ := resp.GetBody()
			fcmResponse := &firebase.Response{}
			json.Unmarshal(b, fcmResponse)
			for k, v := range fcmResponse.Result {
				if _, ok := v["error"]; ok {
					DeleteBroadcast <- unsub.Tokens[k]
				}
			}
		}
	}
}
