package firebase

import (
	"encoding/json"
	"errors"
	"gitlab.com/golang12/push_server/modal/firebase"
	"gitlab.com/golang12/queue_action"
	"gitlab.com/golang12/service/request"
)

func Distribution(message firebase.MessageFirebase, request request.Request) error {
	switch message.GetType() {
	case firebase.PUSH_TOKEN:
		pt := &firebase.PushToken{}
		err := json.Unmarshal(message.GetMsg(),pt)
		if err != nil {
			return err
		} else {
			go PushToken(request, message.GetMsg(), pt)
			return nil
		}
	case firebase.SUBSCRIBE:
		sub := &firebase.Subscribe{}
		err := json.Unmarshal(message.GetMsg(),sub)
		if err != nil {
			return err
		} else {
			asu := &ActionSubUnsub{
				Sub:   sub,
				Unsub: nil,
				Body: message.GetMsg(),
				Request: request,
			}
			go queue.Add(message.GetKey(),asu)
			return nil
		}
	case firebase.PUSH_TOPIC:
		pt := &firebase.PushToken{}
		err := json.Unmarshal(message.GetMsg(),pt)
		if err != nil {
			return err
		} else {
			go PushTopic(request, message.GetMsg())
			return nil
		}
	case firebase.UNSUBSCRIBE:
		unsub := &firebase.Unsubscribe{}
		err := json.Unmarshal(message.GetMsg(),unsub)
		if err != nil {
			return err
		} else {
			asu := &ActionSubUnsub{
				Sub:   nil,
				Unsub: unsub,
				Body: message.GetMsg(),
				Request: request,
			}
			go queue.Add(message.GetKey(),asu)
			return nil
		}
	default:
		return errors.New("invalidate type msg")
	}
}
