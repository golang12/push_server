package firebase

import (
	logs "gitlab.com/golang12/log"
	"gitlab.com/golang12/push_server/modal"
)

var tokens = make([]string,5)
var appendToken string
var DeleteBroadcast = make(chan string)

func HandlerInvalidToken(data map[string]interface{}) {
	output := &logs.OutputFile{Path: "/var/www/push/logs/system.out"}
	log := logs.New(output)
	log.SetFullTrace(false)
	defer log.HandlerError()
	for {
		appendToken = <-DeleteBroadcast
		tokens = append(tokens, appendToken)
		if len(tokens) > 5 {
			modal.DeleteTokens(tokens, data)
			tokens = tokens[:0]
		}
	}
}
