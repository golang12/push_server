package response

import (
	"io/ioutil"
	"net/http"
)

type NetResponse struct {
	Response *http.Response
}

func (nr *NetResponse) GetBody() ([]byte, error) {
	b, err := ioutil.ReadAll(nr.Response.Body)
	if err != nil {
		return nil, err
	} else {
		return b, nil
	}
}

func (nr *NetResponse) GetCode() int {
	return nr.Response.StatusCode
}
