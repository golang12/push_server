package firebase

import (
	"gitlab.com/golang12/push_server/parser"
)

func GetPushToken(tokens []string, title string, body string) ([]byte, error) {
	p := parser.JsonParser{}

	notif := Notification{}
	notif.Body = body
	notif.Title = title

	push := PushToken{}
	push.Notification = notif
	push.Tokens = tokens

	b, e := p.Marshal(push)
	if e != nil {
		panic(e)
	}

	msg := Message{}
	msg.Type = PUSH_TOKEN
	msg.Msg = string(b[:])
	msg.Key = 123

	return p.Marshal(msg)
}

func GetPushTopic(topic string, title string, body string) ([]byte, error) {
	p := parser.JsonParser{}

	notif := Notification{}
	notif.Body = body
	notif.Title = title

	push := PushTopic{}
	push.Notification = notif
	push.Topic = topic

	b, e := p.Marshal(push)
	if e != nil {
		panic(e)
	}

	msg := Message{}
	msg.Type = PUSH_TOPIC
	msg.Msg = string(b[:])
	msg.Key = 123
	return p.Marshal(msg)
}

func GetUnsubscribe(tokens []string, topic string) ([]byte, error) {
	p := parser.JsonParser{}

	sub := Unsubscribe{}
	sub.Topic = topic
	sub.Tokens = tokens

	b, e := p.Marshal(sub)
	if e != nil {
		panic(e)
	}

	msg := Message{}
	msg.Type = UNSUBSCRIBE
	msg.Msg = string(b[:])
	msg.Key = 123
	return p.Marshal(msg)
}

func GetSubscribe(tokens []string, topic string) ([]byte, error) {
	p := parser.JsonParser{}

	sub := &Subscribe{}
	sub.Topic = topic
	sub.Tokens = tokens

	b, e := p.Marshal(sub)
	if e != nil {
		panic(e)
	}

	msg := Message{}
	msg.Type = SUBSCRIBE
	msg.Msg = string(b[:])
	msg.Key = 123
	return p.Marshal(msg)
}
