package firebase

const PUSH_TOKEN = 0
const PUSH_TOPIC = 1
const SUBSCRIBE = 2
const UNSUBSCRIBE =3

type MessageFirebase interface {
	GetMsg() []byte
	GetType() int
	GetKey() int
}
