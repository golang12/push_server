package request

import (
	"bytes"
	"gitlab.com/golang12/push_server/response"
	"gitlab.com/golang12/service/request"
	response2 "gitlab.com/golang12/service/response"
	"io/ioutil"
	"net/http"
	url2 "net/url"
)

type NetRequest struct {
	r *http.Request
}

type GenNetRequest struct{}

func (gen GenNetRequest) New() request.Request {
	return &NetRequest{
		&http.Request{},
	}
}

func (nr *NetRequest) SetUrl(url string) {
	nr.r.URL, _ = url2.ParseRequestURI(url)
}

func (nr *NetRequest) SetMethod(method string) {
	nr.r.Method = method
}

func (nr *NetRequest) SetHeader(header string, value string) {
	if nr.r.Header == nil {
		nr.r.Header = make(http.Header)
	}
	nr.r.Header.Add(header, value)
}

func (nr *NetRequest) SetBody(body []byte) {
	b := bytes.NewBuffer(body)
	r := ioutil.NopCloser(b)
	nr.r.Body = r
}

func (nr *NetRequest) Do() (error, response2.Response) {
	c := http.Client{}
	r := &response.NetResponse{}
	resp, err := c.Do(nr.r)
	r.Response = resp
	if err != nil {
		return err, r
	} else {
		r.Response = resp
		return nil, r
	}
}
