package parser

import (
	"errors"
	"github.com/mailru/easyjson"
	"github.com/mailru/easyjson/jlexer"
)

type JsonParser struct {
}

func (JsonParser) Unmarshal(b []byte, v interface{}) error {
	r := jlexer.Lexer{Data: b}
	if v, ok := v.(easyjson.Unmarshaler); ok {
		v.UnmarshalEasyJSON(&r)
		err := r.Error()
		if err != nil {
			return err
		} else {
			return nil
		}
	} else {
		return errors.New("the parameter must implement easyjson.Unmarshaler")
	}
}

func (JsonParser) Marshal(m interface{}) ([]byte, error) {
	if m, ok := m.(easyjson.Marshaler); ok {
		return easyjson.Marshal(m)
	} else {
		return nil, errors.New("the parameter must implement easyjson.Marshaler")
	}
}
